# Running the application inside docker

The last pipeline step of projects itinerary-service and route-service is to build and push the docker image of the application. This solution is using a public docker registry inside gitlab.

Here, it is explained how to execute the application using docker. All docker solutions below are using **postgresql database**.


# Docker compose

This is the easiest way to start the application. It is only necessary to have installed docker and docker-compose. In the root of this repository, exists a file called 'docker-compose.yml', where all containers are declared. To start the application run the following command: 

    $ docker-compose up

- itinerary-service url: [http://localhost:8080/itineraries/](http://localhost:8080/itineraries/)
- route-service url: [http://localhost:8081/routes/less-time?origin=1&destiny=3](http://localhost:8081/routes/less-time?origin=1&destiny=3)

# Kubernetes documentation

**Note:** To deploy this application on Minikube (kubernetes) follow these [instructions](https://kubernetes.io/docs/tasks/tools/install-minikube/).

All kubernetes deployments files are in this repository on the folder 'kubernetes'.


To deploy this application on kubernetes, run the following command:

    $ kubectl apply -f kubernetes/postgresql-deployment.yml
    $ kubectl apply -f kubernetes/itinerary-service-deployment.yml
    $ kubectl apply -f kubernetes/route-service-deployment.yml


To access the application itinerary-service execute the following command and add at the end of URL "/itineraries/":

    $ minikube service itinerary-app

To access the application itinerary-service execute the following command and add at the end of URL "/routes/less-time?origin=1&destiny=3":

    $ minikube service route-app


**Notes:** 
- The database is inside docker, I know that it is not a good practice, it's only there for the facility. [CloudSQL on Kubernetes(GKE)](https://cloud.google.com/sql/docs/mysql/connect-kubernetes-engine)
- The best way to store the username and password for the database is inside kubernetes secret. 

